const {user} = require('../data'); //mockdata for our users

module.exports = (app) => {
	app.get('/', (req, res) => {
		return res.status(200).send()
	})

	//GET API
	app.get('/users', (req, res) => {
		return res.send(users);
	})

	app.post('/user', (req, res) => {

		if(!req.body.hasOwnProperty('fullName')){
			return res.status(400).send({
				'Error': "Bad request - missing required parameter fullname"
			})
		}

		return res.send(200).send({
			'Message': 'User created!'
		})
	})

}